upstream consul {
{{- range service "consul" }}
        server {{ .Address }}:8501;
{{- end }}
}

server {
	server_name consul.ifoodorder.com;
	resolver 127.0.0.1 valid=30s;
	location / {
		proxy_pass	https://consul;
		proxy_set_header    Host               $host;
		proxy_set_header    X-Real-IP          $remote_addr;
		proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
		proxy_set_header    X-Forwarded-Host   $host;
		proxy_set_header    X-Forwarded-Server $host;
		proxy_set_header    X-Forwarded-Port   $server_port;
		proxy_set_header    X-Forwarded-Proto  $scheme;

		proxy_ssl_certificate certs/cert.pem;
		proxy_ssl_certificate_key certs/cert.key;
		proxy_ssl_trusted_certificate certs/ca.crt;
	}
	access_log /var/log/nginx/consul_access.log;
        error_log /var/log/nginx/consul_error.log;

	listen [::]:443 ssl ipv6only=on http2; # managed by Certbot
	listen 443 ssl http2; # managed by Certbot
	ssl_certificate /etc/letsencrypt/live/consul.ifoodorder.com/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/consul.ifoodorder.com/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
	if ($host = consul.ifoodorder.com) {
		return 301 https://$host$request_uri;
	} # managed by Certbot


	listen 80;
	listen [::]:80;
	server_name consul.ifoodorder.com;
	return 404; # managed by Certbot
}