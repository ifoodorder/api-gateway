upstream graylog {
{{- range service "graylog" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

server {
	server_name graylog.dev.ifoodorder.com;
	resolver 127.0.0.1 valid=30s;

	location / {
		proxy_pass	https://graylog;
		proxy_set_header	Host	$host;
		proxy_set_header    X-Real-IP	$remote_addr;
		proxy_buffering off;
		gzip off;
	}

	access_log /var/log/nginx/graylog_access.log;
	error_log /var/log/nginx/graylog_error.log;

	listen 443 ssl; # managed by Certbot
	ssl_certificate /etc/letsencrypt/live/graylog.dev.ifoodorder.com/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/graylog.dev.ifoodorder.com/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
	if ($host = graylog.dev.ifoodorder.com) {
		return 301 https://$host$request_uri;
	} # managed by Certbot
	server_name graylog.dev.ifoodorder.com;
	listen 80;
	return 404; # managed by Certbot
}
