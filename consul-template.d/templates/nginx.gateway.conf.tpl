upstream frontend {
{{- range service "frontend" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

upstream menu-service {
{{- range service "menu-service" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

upstream user-service {
{{- range service "user-service" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

upstream restaurant-service {
{{- range service "restaurant-service" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

upstream order-service {
{{- range service "order-service" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}    
}

server {
	server_name test.system.ifoodorder.com
	resolver 127.0.0.1 valid=30s;

	location /api/users {
		proxy_pass https://user-service;
		proxy_set_header	Host	$host;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_buffering off;
	}

	location /api/menu {
		proxy_pass https://menu-service;
		proxy_set_header	Host	$host;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_buffering off;
	}

	location /api/orders {
		proxy_pass https://order-service;
		proxy_set_header	Host	$host;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_buffering off;
	}

	location /api/restaurants {
		proxy_pass https://restaurant-service;
		proxy_set_header	Host	$host;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_buffering off;
	}

	location / {
		proxy_pass https://frontend;
		proxy_set_header	Host	$host;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_buffering off;
	}

	access_log /var/log/nginx/gateway_access.log;
	error_log /var/log/nginx/gateway_error.log;

	listen 443 ssl; # managed by Certbot
	ssl_certificate /etc/letsencrypt/live/test.system.ifoodorder.com/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/test.system.ifoodorder.com/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
	if ($host = test.system.ifoodorder.com) {
		return 301 https://$host$request_uri;
	} # managed by Certbot
	server_name test.system.ifoodorder.com;
	listen 80;
	return 404; # managed by Certbot
}