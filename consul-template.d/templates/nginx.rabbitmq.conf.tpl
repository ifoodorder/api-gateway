upstream rabbitmq {
{{- range service "rabbitmq-management" }}
	server {{ .Address }}:{{ .Port }};
{{- end }}
}

server {
	server_name rabbitmq.ifoodorder.com;
	resolver 127.0.0.1 valid=30s;

	location / {
		proxy_pass https://rabbitmq;
		proxy_set_header    Host    $host;
		proxy_set_header    X-Real-IP   $remote_addr;
		proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header    X-Forwarded-Host    $host;
		proxy_set_header    X-Forwarded-Server  $host;
		proxy_set_header    X-Forwarded-Port    $server_port;
		proxy_set_header    X-Forwarded-Proto   $scheme;
		proxy_redirect  http://rabbitmq.ifoodorder.com/  https://rabbitmq.ifoodorder.com/;
		proxy_http_version 1.1;
		proxy_buffering off;
		gzip off;
	}

	access_log /var/log/nginx/rabbitmq_access.log;
	error_log /var/log/nginx/rabbitmq_error.log;

	listen 443 ssl http2; # managed by Certbot
	ssl_certificate /etc/letsencrypt/live/rabbitmq.ifoodorder.com/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/rabbitmq.ifoodorder.com/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot	
}
server {
	if ($host = rabbitmq.ifoodorder.com) {
		return 301 https://$host$request_uri;
	} # managed by Certbot

    server_name rabbitmq.ifoodorder.com;
    listen 80;
    return 404; # managed by Certbot
}
