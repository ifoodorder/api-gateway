syslog {
  enabled = true
  facility = "LOCAL5"
}

template {
  source = "templates/nginx.keycloak.conf.tpl"
  destination = "/etc/nginx/conf.d/keycloak.conf"
}
template {
  source = "templates/nginx.wiki.conf.tpl"
  destination = "/etc/nginx/conf.d/wiki.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.weblate.conf.tpl"
  destination = "/etc/nginx/conf.d/weblate.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.rabbitmq.conf.tpl"
  destination = "/etc/nginx/conf.d/rabbitmq.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.pgadmin.conf.tpl"
  destination = "/etc/nginx/conf.d/pgadmin.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.wordpress.conf.tpl"
  destination = "/etc/nginx/conf.d/wordpress.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.phpmyadmin.conf.tpl"
  destination = "/etc/nginx/conf.d/phpmyadmin.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.graylog.conf.tpl"
  destination = "/etc/nginx/conf.d/graylog.conf"
  command = "systemctl reload nginx.service"
}
template {
  source = "templates/nginx.consul.conf.tpl"
  destination = "/etc/nginx/conf.d/consul.conf"
  command = "systemctl reload nginx.service"
}
