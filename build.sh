#!/bin/sh
WORKDIR=$(dirname $0)
mkdir -p ${WORKDIR}/dist/consul-template.d
cp -r ${WORKDIR}/consul-template.d/templates ${WORKDIR}/dist/consul-template.d
cp ${WORKDIR}/consul-template.d/nginx.hcl ${WORKDIR}/dist/consul-template.d/nginx.hcl